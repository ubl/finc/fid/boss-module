<? $script = <<<JS
  $(document).ready(function() {
      var recordId = $('.hiddenId').val();
      var recordSource = $('.hiddenSource').val();
      // console.log({id: recordId, source: recordSource});
      $.ajax({
        dataType: 'json',
        url: VuFind.path + '/AJAX/JSON?method=getBoss',
        method: 'GET',
        data: {id: recordId, source: recordSource}
      }).done(function(response) {
          $('.boss').html(response.data.html);
      });
    });
JS;
?>
<?=$this->inlineScript(\Laminas\View\Helper\HeadScript::SCRIPT, $script, 'SET');?>
<div class="boss"></div>