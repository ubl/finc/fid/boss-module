<?php
/**
 * Copyright (C) Leipzig University Library 2019.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * @author   Gregor Gawol <gawol@ub.uni-leipzig.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU GPLv2
 */

namespace finc\Boss\Client;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use VuFindHttp\HttpServiceInterface as HttpService;

/**
 * "Get Boss data" AJAX Handler
 *
 * This service will retrieve the data of webservice BSZ One Stop Search (BOSS)
 * to display avaiabiblity of a certain record
 *
 * @package finc\Boss\Client
 * @author  Gregor Gawol <gawol@ub.uni-leipzig.de>
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 */
class BossClient
{
    protected const BOSS_URL = 'https://fernleihe.boss.bsz-bw.de/Holding/Query';

    /**
     *
     * @var HttpService
     */
    protected $httpService;

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * Client constructor.
     *
     * @param HttpService             $httpService
     * @param SerializerInterface     $serializer
     */
    public function __construct(
        HttpService $httpService,
        SerializerInterface $serializer
    ) {
        $this->httpService = $httpService;
        $this->serializer = $serializer;
    }

    /**
     * Retrieve Worldcat search webservice with ISBN or ISSN
     *
     * @param $isxns
     * @param $network
     * @return array
     * @throws ClientException
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    public function getRequestISBN($isxns, $network)
    {
        $retval = [];
        foreach ($isxns as $isxn) {
            
            $uri = static::BOSS_URL . "?isxn[]=$isxn&network=$network";
            $client = $this->httpService->createClient(
                $uri
            );

            /* @var \Laminas\Http\Response $response */
            $response = $client->send();

            if ($response->getStatusCode() == "200") {
                $data = $this->lookupData($response);
                if (!empty($data->getHoldings())) {

                    $retval['data'] = $data->getHoldings();
                    $retval['param'] = $isxn;
                    return $retval;
                }
            }
        }
        return $retval;
    }

    /**
     * Retrieve Worldcat search webservice with zdb id
     *
     * @param $zdb
     * @param $network
     * @return array|void
     * @throws ClientException
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    public function getRequestZDB($zdb, $network)
    {
        $retval = [];
        $uri = static::BOSS_URL . "?zdb=$zdb&network=$network";
        $client = $this->httpService->createClient(
            $uri
        );
        
        /* @var \Laminas\Http\Response $response */
        $response = $client->send();

        if ($response->getStatusCode() == "200") {
            $retval['data'] = $this->lookupData($response)->getHoldings();
            $retval['param'] = $zdb;
            return $retval;
        }
        return $retval;
    }

    /**
     * Retrieve Worldcat search webservice with query params
     * author, title, year
     *
     * @param $author
     * @param $title
     * @param $year
     * @param $network
     * @return array
     * @throws ClientException
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    public function getRequestQuery($author, $title, $year, $network)
    {
        $uri = static::BOSS_URL . "?author=$author&title=$title&year=$year&network=$network";
        $client = $this->httpService->createClient(
            $uri
        );
        
        /* @var \Laminas\Http\Response $response */
        $response = $client->send();

        if ($response->getStatusCode() == "200") {
            $retval['data'] = $this->lookupData($response)->getHoldings();
            $retval['param'] = '';
            return $retval;
        }
        return [];
    }

    /**
     * Transform retrieving Response data (JSON Object) to PHP classes
     *
     * @param $response
     * @return Root
     */
    private function lookupData($response): Root
    {
        /** @var Root $root */
        $root = $this->serializer->deserialize(
            (string)$response->getBody(), Root::class, 'json', [
            ObjectNormalizer::class => true,
            JsonEncoder::class => true,
        ]);
        return $root;
    }
}
