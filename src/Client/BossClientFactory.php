<?php
/**
 * Copyright (C) Leipzig University Library 2019.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author   Gregor Gawol <gawol@ub.uni-leipzig.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 */
namespace finc\Boss\Client;

use Psr\Container\ContainerInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * "Get Boss data" AJAX Handler
 *
 * This service will retrieve the data of webservice BSZ One Stop Search (BOSS)
 * to display avaiabiblity of a certain record
 *
 * @package finc\Boss\Client
 * @author  Gregor Gawol <gawol@ub.uni-leipzig.de>
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 */
class BossClientFactory
{
    /**
     * Create an object
     *
     * @param ContainerInterface $container
     * @return mixed|object
     * @throws \Exception
     */
    public function __invoke(ContainerInterface $container)
    {
        return new BossClient(
            $container->get(\VuFindHttp\HttpService::class),
            $container->get(SerializerInterface::class)
        );
    }
}
