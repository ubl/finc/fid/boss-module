<?php
/**
 * Copyright (C) 2018 Leipzig University Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * @author   Gregor Gawol <gawol@ub.uni-leipzig.de>
 * @author   Sebastian Kehr <kehr@ub.uni-leipzig.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU GPLv2
 */

namespace finc\Boss\Client;

/**
 * JSON Mapping Class Root
 *
 * @package  finc\Boss\Client
 * @author   Gregor Gawol <gawol@ub.uni-leipzig.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
class Root
{
    /**
     * @var Holding[][]
     */
    protected $holdings = [];
    /**
     * @var int
     */
    protected $numppn = 0;
    /**
     * @var int
     */
    protected $numfound = 0;

    /**
     * @param Holding[] $holding
     */
    public function addHolding(Holding $holding): void
    {
        $this->holdings[] = $holding;
    }

    /**
     * @return Holding[][]
     */
    public function getHoldings(): array
    {
        return $this->holdings;
    }

    /**
     * @param Holding[][] $holdings
     */
    public function setHoldings(array $holdings): void
    {
        $this->holdings = $holdings;
    }

    /**
     * @return int
     */
    public function getNumppn(): ?int
    {
        return $this->numppn;
    }

    /**
     * @param int $numppn
     */
    public function setNumppn(?int $numppn): void
    {
        $this->numppn = $numppn;
    }

    /**
     * @return int
     */
    public function getNumfound(): ?int
    {
        return $this->numfound;
    }

    /**
     * @param int $numfound
     */
    public function setNumfound(?int $numfound): void
    {
        $this->numfound = $numfound;
    }
}
