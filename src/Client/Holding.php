<?php
/**
 * Copyright (C) 2018 Leipzig University Library
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * @author   Gregor Gawol <gawol@ub.uni-leipzig.de>
 * @author   Sebastian Kehr <kehr@ub.uni-leipzig.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU GPLv2
 */

namespace finc\Boss\Client;

/**
 * JSON Mapping Class Holding
 *
 * @package  finc\Boss\Client
 * @author   Gregor Gawol <gawol@ub.uni-leipzig.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 * @link     https://vufind.org/wiki/development Wiki
 */
class Holding
{
    /**
     * @var string
     */
    protected $isil = '';
    /**
     * @var string|array
     */
    protected $callnumber = '';
    /**
     * @var string
     */
    protected $issue = '';

    /**
     * @return string
     */
    public function getIsil(): string
    {
        return $this->isil;
    }

    /**
     * @param string $isil
     */
    public function setIsil(string $isil): void
    {
        $this->isil = $isil;
    }

    /**
     * @return string
     */
    public function getCallnumber(): string
    {
        return $this->callnumber;
    }
    
    /**
     * @param $callnumber
     */
    public function setCallnumber(string|array $callnumber): void {
        if (is_array($callnumber)) {
            $this->callnumber = $callnumber[0];
        } else {
            $this->callnumber = $callnumber;
        }
    }

    /**
     * @return string
     */
    public function getIssue(): string
    {
        return $this->issue;
    }

    /**
     * @param string $issue
     */
    public function setIssue(string $issue): void
    {
        $this->issue = $issue;
    }
}
