<?php
/**
 * Copyright (C) Leipzig University Library 2019.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @author   Gregor Gawol <gawol@ub.uni-leipzig.de>
 * @license  http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 */
namespace finc\Boss\AjaxHandler;

use fid\Service\Client;
use finc\Boss\Client\BossClient;
use VuFind\Record\Loader;
use Laminas\Config\Config;
use Laminas\Http\PhpEnvironment\Request;
use Laminas\Mvc\Controller\Plugin\Params;
use Laminas\View\Renderer\RendererInterface;

/**
 * "Get Boss data" AJAX Handler
 *
 * This service will retrieve the data of webservice BSZ One Stop Search (BOSS)
 * to display the availability of a certain record
 *
 * @package finc\Boss\AjaxHandler
 * @author  Gregor Gawol <gawol@ub.uni-leipzig.de>
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License
 */
class GetBoss extends \VuFind\AjaxHandler\AbstractBase
{

    /**
     * Top-level configuration
     *
     * @var Config
     */
    protected $config;

    /**
     * HTTP Service
     *
     * @var BossClient
     */
    protected $bossClient;

    /**
     * Request
     *
     * @var Request
     */
    protected $request;

    /**
     * Record loader
     *
     * @var Loader
     */
    protected $recordLoader;

    /**
     * FID API Client
     *
     * @var Client
     */
    protected $fidClient;

    /**
     * View renderer
     *
     * @var RendererInterface
     */
    protected $renderer;

    /**
     * Constructor
     *
     * @param BossClient            $bossClient     http client
     * @param Config                $config         Top-level configuration
     * @param Loader                $loader         VuFind Record Loader
     * @param Request               $request        Request
     * @param Client                $fidClient      Resolver Library Mapper
     * @param RendererInterface     $renderer       view
     */
    public function __construct(
        BossClient $bossClient,
        Config $config,
        Loader $loader,
        Request $request,
        Client $fidClient,
        RendererInterface $renderer
    ) {
        $this->bossClient = $bossClient;
        $this->config = $config;
        $this->recordLoader = $loader;
        $this->request = $request;
        $this->fidClient = $fidClient;
        $this->renderer = $renderer;
    }

    /**
     * Handle a request.
     *
     * @param Params $params Parameter helper from controller
     *
     * @return array [response data, HTTP status code]
     */
    public function handleRequest(Params $params)
    {
        if (!isset($this->config->General)) {
            return $this->formatResponse(
                'Configuration file boss.ini not found',
                self::STATUS_HTTP_BAD_REQUEST
            );
        }

        if ($this->fidClient->isLoggedOn()) {
            $user = $this->fidClient->requestUserDetails();
            $libraries = $this->fidClient->requestLibraryList();
            $homeLibrary = $user->getHomeLibrary();
            // Obtain user information from ILS:
            $library = isset($libraries[$homeLibrary]) ?
                $libraries[$homeLibrary] : $libraries['AAAAA'];
            $network = strtolower($library->getNetwork());
            $isil = $library->getIsil();

            $driver = $this->recordLoader->load(
                $params->fromQuery('id'),
                $params->fromQuery('source')
            );
            $seq = isset($this->config->Search->sequence) ?
                $this->config->Search->sequence : [];
            $fields = $driver->getRawData();
            $results = false;
            foreach ($seq as $data) {
                if (isset($fields[$data])) {
                    $method = $this->config->Search->$data;
                    $value = (array)$driver->tryMethod($method);
                    $methodName = "getRequest" . strtoupper($data);
                    $results = $this->bossClient->$methodName(
                        $value, strtoupper($network)
                    );
                    if (!empty($results['data'])) {
                        break;
                    }
                }
            }
            // default search of boss
            if (empty($results)) {
                $author = $driver->tryMethod('getCombinedAuthors');
                $author = count($author) > 0 ? $author[0] : '';
                $title = $driver->tryMethod('getTitle');
                $year = $driver->tryMethod('getPublishDateSort');
                $results = $this->bossClient->getRequestQuery(
                    $author, $title, $year, strtoupper($network)
                );
            }

            $isilCallNumber = $this->getISILCallNumber($results['data']);
            $networks = isset($this->config->General->networks) ?
                $this->config->General->networks->toArray() : [];
            $addNetworks = isset($this->config->General->addNetworks) ?
                $this->config->General->addNetworks->toArray() : [];
            $inArray = in_array($network, $networks);
            $zdbId = $driver->tryMethod('getZdbId');
            $isbns = $driver->tryMethod('getISBNs');
            $isbns = count($isbns) > 0 ? $isbns[0] : '';
            $results['param'] = !empty($results['param']) ? $results['param'] : $isbns;
            $isISXNZBD = (!empty($isbns) || !empty($zdbId)) ? true : false;
            $callnumber = $this->getCallnumbers($isil, $isilCallNumber);

            $isISIL = $inArray && $isISXNZBD ? key_exists($isil, $isilCallNumber): false;

            $view = [
                'homeLibrary' => ($homeLibrary == "AAAAA") ? true : false,
                'isISXNZBD' => $isISXNZBD,
                'isISIL' => $isISIL,
                'isAddNetwork' => in_array($network, $addNetworks) ? true : false,
                'url' => sprintf($this->config->SearchUrls->$network, $results['param']),
                'callnumber' => $callnumber
            ];

            $html = $this->renderer->render('ajax/boss.phtml', $view);
            return $this->formatResponse(compact('html', 'isISIL'));
        }
        return $this->formatResponse(
            'No Auth',
            self::STATUS_HTTP_NEED_AUTH
        );
    }

    /**
     * Get the call numbers of a certain isil
     *
     * @param $isil - ISIL of the library, like DE-15
     * @param $data - Array of CallNumbers with Key:Value equal to ISIL:Number (and sometimes ISIL:[Number])
     *
     * @return array of deduplicated callnumbers
     */
    private function getCallnumbers($isil, $data)
    {
        if ( !array_key_exists($isil, $data) ) {return [];}
        // somestimes the value is just a value, sometimes its an array of value, untidy!
        if ( !is_array($data[$isil])) { $data[$isil] = [$data[$isil]]; }

        $cnValue = [];
        foreach ( $data[$isil] as $number) {
            if (!empty($number) && !in_array($number, $cnValue)) {
                $cnValue[] = $number;
            }
        }
        return $cnValue;
    }

    /**
     * Get all call numbers with array structure
     * [ isil => call number]
     *
     * @param array $raw_data as it comes from the interface
     * usually [Holding => [ 0 => ['isil', 'callnumber', 'issue'] ] ]
     *
     * @return array of key:value as ISIL: [Callnumber], Callnumbers may be singular or multivalued
     */
    private function getISILCallNumber($raw_data)
    {
        $retval = [];
        $raw_data = is_null($raw_data) ? [] : $raw_data;
        foreach ($raw_data as $row) {
            foreach ($row as $holding) {
                $retval[$holding->getIsil()] = $holding->getCallnumber() ?? '';
            }
        }

        return $retval;
    }

}
