# VuFind Boss Module

Introduction
------------

This module is an Open Source Software of the University Library Leipzig.
The module retrieve the data of [BSZ One Stop Search (BOSS)](https://wiki.bsz-bw.de/doku.php?id=projekte:boss:start).

Requirements
------------

Only Records with
* ISBN/ISSN
* ZDB ID

will be retrieve the boss data.

Installation
------------

**Composer**

Packagist Repo [Module for BOSS Webservice](https://packagist.org/packages/finc/boss-module)

To Install use Composer
    
    php composer require finc\boss-module
    
**VuFind**

*theme.config.php*

Write to the configuration a *mixins*-statement.

```
return [
  [...],
  'mixins' => [
    'boss'
  ],
  [...]
];
```

*templates/**file**.phtml*

To display the data when write the following JS-Script in to a PHTML-File

```
<? $script = <<<JS
$(document).ready(function() {
  var recordId = $('.hiddenId').val();
  var recordSource = $('.hiddenSource').val();
  $.ajax({
    dataType: 'json',
    url: VuFind.path + '/AJAX/JSON?method=getBoss',
    method: 'GET',
    data: {id: recordId, source: recordSource}
  }).done(function(response) {
    $('.boss-data').html(response.data.html);
  });
});
JS;
?>
<?=$this->inlineScript(\Laminas\View\Helper\HeadScript::SCRIPT, $script, 'SET');?>
<div class="boss-data"></div>
```

Configuration
-------------

You must to create a configuration file in config folder.

**boss.ini**
    
    ; Which networks do you want to retrieve data?
    [General]
    networks[] = swb
    networks[] = gbv
    networks[] = bvb
    networks[] = hebis
    networks[] = hbz
    networks[] = kobv
    ; These are additional networks
    addNetworks[] = öbv
    addNetworks[] = swissbib
    
    ; search urls to the german/swiss network catalogs
    ; if no data to a certain record when you can display this
    [SearchUrls]
    swb = "http://swb.bsz-bw.de/DB=2.1/SET=1/TTL=1/CMD?ACT=SRCHA&IKT=1007&TRM=%s"
    gbv = "https://gso.gbv.de/DB=2.1/SET=1/TTL=1/CMD?ACT=SRCHA&IKT=1007&TRM=%s"
    bvb = "https://opacplus.bib-bvb.de/TouchPoint_touchpoint/search.do?methodToCall=submitButtonCall&methodToCallParameter=submitSearch&refine=false&searchCategories[0]=-1&speedy=on&searchString[0]=%s"
    hebis = "http://cbsopac.rz.uni-frankfurt.de/DB=2.1/SET=9/TTL=1/CMD?COLMODE=1&ACT=SRCHA&IKT=8520&SRT=YOP&TRM=%s"
    hbz = "http://193.30.112.134/F/?func=find-c&ccl_term=IBN=%s"
    kobv = "http://portal.kobv.de/simpleSearch.do?query=%s&plv=2"
    swissbib = "https://www.swissbib.ch/Search/Results?lookfor=%s&type=ISN"
    öbv = "http://search.obvsg.at/primo_library/libweb/action/search.do?fn=search&ct=search&initialSearch=true&mode=Basic&tab=default_tab&indx=1&dum=true&srt=rank&vid=OBV&frbg=&tb=t&vl(freeText0)=%s&scp.scps=scope=(OBV_aleph)"
    
    ; Which attributes to search with
    ; you can search with isbn/issn or zdb attribute
    [Search]
    sequence[] = zdb
    sequence[] = isbn
    zdb = getZdbId
    isbn = getISBNs